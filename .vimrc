call plug#begin()
Plug 'git@github.com:kien/ctrlp.vim.git'
Plug 'git@github.com:frazrepo/vim-rainbow.git'
Plug 'git@github.com:yangmillstheory/vim-snipe.git'
Plug 'git@github.com:tpope/vim-vinegar.git'
Plug 'git@github.com:Yggdroot/indentLine.git'
Plug 'git@github.com:davidhalter/jedi-vim'
Plug 'git@github.com:jmcantrell/vim-virtualenv.git'
Plug 'git@github.com:preservim/tagbar.git'
Plug 'git@github.com:dense-analysis/ale'
Plug 'git@github.com:fatih/vim-go.git'
Plug 'git@github.com:jiangmiao/auto-pairs.git'
Plug 'git@github.com:sjl/badwolf.git'
Plug 'git@github.com:tpope/vim-surround.git'
Plug 'git@github.com:tpope/vim-fugitive.git'
Plug 'git@github.com:gcmt/taboo.vim.git'
call plug#end()

set nocompatible
set background=dark
set number
set relativenumber
set noswapfile
set showcmd
set hlsearch
set smartcase
set ignorecase
set nostartofline
set confirm
set cmdheight=2
set autoread
set notimeout ttimeout ttimeoutlen=200
set nobackup "save on clutterish backup files
set tabstop=3 "indent using a tab size of 2 spaces
set softtabstop=3 "^^^
set shiftwidth=3 "^^^
set expandtab
set autoindent "^^^
set autoread
set cursorline
set t_Co=256

"=============================
"====== SYNTAX SETTINGS ======
"============================

syntax on "syntax highlighting
syntax sync minlines=256 "Life saving for large files and slow machines
set synmaxcol=2048 "same as minlines but for how far out to hightlight lines
set autoindent "keep code pretty and indented
set smartindent "be even better about keeping code pretty

"=========================================
"====== MODIFY TERMINAL ENVIRONMENT ======
"=========================================

if &term =~ '256color'
  " disable Background Color Erase (BCE) so that color schemes
  " render properly when inside 256-color tmux and GNU screen.
  " see also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
endif


colorscheme badwolf

let g:virtualenv_directory = '/home/jacob/code/venv'

let b:ale_linters = {'python' : ['flake8', 'pylint'],'go' : ['staticcheck', 'gofmt']}
let b:ale_fixers = {'python' : ['autoflake']}
let g:rainbow_active = 1

"=================
"===== snipe =====
"=================
map f <Plug>(snipe-f)
map F <Plug>(snipe-F)

"==================
"===== tagbar =====
"==================
let g:tagbar_position='topleft vertical'
let g:tagbar_type_yaml = {
    \ 'ctagstype' : 'yaml',
    \ 'kinds' : [
        \ 'a:anchors',
        \ 's:section',
        \ 'e:entry'
    \ ],
  \ 'sro' : '.',
    \ 'scope2kind': {
      \ 'section': 's',
      \ 'entry': 'e'
    \ },
    \ 'kind2scope': {
      \ 's': 'section',
      \ 'e': 'entry'
    \ },
    \ 'sort' : 0
    \ }
"=====================================================
"===== Fixes for some debugger oddities w/ delve =====
"=====================================================
autocmd BufEnter * silent! lcd %:p:h

"===========================
"==== Tabbar colouring =====
"===========================
hi TabLine ctermfg=Black ctermbg=Red
hi TablineSel ctermfg=Yellow ctermbg=Red

"======================
"==== Transparency ====
"======================
hi Normal guibg=NONE ctermbg=NONE
hi EndOfBuffer guibg=NONE ctermBG=NONE
