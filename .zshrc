#############################################################################################################
# Autoconfigurations
#############################################################################################################

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/home/jacob/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=2000
setopt appendhistory autocd
bindkey -v

#############################################################################################################
# User set options
#############################################################################################################
autoload -U promptinit
setopt prompt_subst

#############################################################################################################
# Environment variables
#############################################################################################################
export GOPATH=$HOME/gojects
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/home/jacob/.local/bin
export PATH=$PATH:/home/jacob/.bin
export VISUAL=vim
export EDITOR="$VISUAL"

#############################################################################################################
# Build Prompt
##############################################################################################################
branch_symbol=''
##fn to check for git repo
is_git_repo(){
  git rev-parse --is-inside-work-tree >/dev/null 2> /dev/null && return 0 || return 1;
}

get_branch(){
  if is_git_repo; then
    echo $branch_symbol$(git branch 2> /dev/null | grep \* | cut -d ' ' -f2)
  else
    echo ''
  fi
}

get_project(){
  if is_git_repo; then
    echo $(basename `git rev-parse --show-toplevel` 2> /dev/null)
  else
    echo ''
  fi
}

get_bits (){
if is_git_repo; then
  git status 2> /dev/null | (
      unset dirty deleted untracked newfile ahead renamed
      while read line ; do
        case "$line" in
          *modified:*)                      dirty='M' ; ;;
          *deleted:*)                       deleted='D' ; ;;
          *'Untracked files:')              untracked='UF' ; ;;
          *'new file:'*)                    newfile='NF' ; ;;
          *'Your branch is ahead of '*)     ahead='->' ; ;;
          *renamed:*)                       renamed='R' ; ;;
        esac
      done
      bits="$dirty$deleted$untracked$newfile$ahead$renamed"
      [ -n "$bits" ] && echo "λ$bits" || echo ''
    )
fi
}

LEFT_ARROW=
RIGHT_ARROW=

segment(){
  if [ "$3" = "fin" ]; then
    echo "%F{016}%K{$1}$2%F{$1}%k$LEFT_ARROW%k%f%b"
  else
    echo "%F{016}%K{$1}$2%F{$1}%K{$3}$LEFT_ARROW%k%f%b"
  fi
}
#user@host
SEG1=' %n@%M%F'
SEG1_BG=124
#directory
SEG2=' %~ '
SEG2_BG=160

#git project and branch
SEG3='$(get_project)$(get_branch)'
SEG3_BG=202

#git status
SEG4='$(get_bits)'
SEG4_BG=208

CMD_SEG=$'\n''$ '
CMD_SEG_BG=214

SEGMENT1=$(segment $SEG1_BG $SEG1 $SEG2_BG)
SEGMENT2=$(segment $SEG2_BG $SEG2 $SEG3_BG)
SEGMENT3=$(segment $SEG3_BG $SEG3 $SEG4_BG)
SEGMENT4=$(segment $SEG4_BG $SEG4 fin)
CMD_SEG=$(segment $CMD_SEG $CMD_SEG_BG $CMD_SEG_BG)

PROMPT="%k%b%f%K{$SEG1_BG}%F{016}$LEFT_ARROW%k%f%b"$SEGMENT1$SEGMENT2$SEGMENT3$SEGMENT4'%k%f'$'\n''%K{$CMD_SEG_BG}%F{016}'$LEFT_ARROW'%K{$CMD_SEG_BG}%F{016}$%F{$CMD_SEG_BG}%K{default}'$LEFT_ARROW'%k%f'
promptinit
#because I'm to lazy to find the cause of the unterminated line
unsetopt prompt_cr prompt_sp
#############################################################################################################
#Env Loading
#############################################################################################################
if [[ -a /opt/env/.zshrc ]]; then
  source /opt/env/.zshrc
fi

##############################################################################################################
## tmux stuff
##############################################################################################################
#parse_git_for_tmux (){
#if is_git_repo; then
#  git status 2> /dev/null | (
#      unset dirty deleted untracked newfile ahead renamed
#      while read line ; do
#        case "$line" in
#          *modified:*)                      dirty='M' ; ;;
#          *deleted:*)                       deleted='D' ; ;;
#          *'Untracked files:')              untracked='UF' ; ;;
#          *'new file:'*)                    newfile='NF' ; ;;
#          *'Your branch is ahead of '*)     ahead='->' ; ;;
#          *renamed:*)                       renamed='R' ; ;;
#        esac
#      done
#      bits="$dirty$deleted$untracked$newfile$ahead$renamed"
#      [ -n "$bits" ] && echo "λ$bits" || echo 'λ'
#    )
#fi
#}
#
#tmux_status_bar(){
#  RIGHT_STATUS_BAR="%H:%M:%S"
#  LEFT_STATUS_BAR="#[fg=colour124,bg=default]#[fg=colour16,bg=colour124,bold] #S #[fg=colour124,bg=default,nobold]#[fg=colour160]#[fg=colour233,bg=colour160] #(whoami) #[fg=colour160,bg=default]#[fg=colour202]#[fg=colour235,bg=colour202] #I:#P #[fg=colour202,bg=default]"
##
#  if [[ ! -z "$TMUX" ]]; then
#    tmux set-window-option status-right "$RIGHT_STATUS_BAR"
#    tmux set-window-option status-left "$LEFT_STATUS_BAR"
# fi
#}
#
##------------------------------------------------------------------------------------------------------------
## Tmux Status Bar functions
##------------------------------------------------------------------------------------------------------------
#
#[[ $- != *i* ]] && return
#[[ -z "$TMUX" ]] && exec tmux
#precmd_functions=(tmux_status_bar)


export PATH="$PATH:/usr/local/go/bin:/home/jacob/.local/bin:$HOME/.tfenv/bin"

# kc auto complete
alias kc='kubectl --kubeconfig ~/.kube/config'
source <(kc completion zsh) # setup autocomplete in bash into the current shell, bash-completion package should be installed first.
source <(kubectl completion zsh)

if [ -z "$SSH_AGENT_PID" ]; then
   eval $(ssh-agent) > /dev/null
fi
