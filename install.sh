#!/bin/bash
if [ ! "$USER" == "root" ]; then
   echo "Run with sudo"
   exit 1
fi

copy_and_chown(){
   cp $1 $2
   chown $SUDO_USER:$SUDO_USER $2 
}

### Copy Over Configs ###

copy_and_chown .zshrc /home/$SUDO_USER/.zshrc
copy_and_chown .tmux.conf /home/$SUDO_USER/.tmux.conf
copy_and_chown .vimrc /home/$SUDO_USER/.vimrc


### INSTALL TOOLS ###
#### ZSH ####
if [ ! $(command -v zsh) ]; then
   apt install zsh -y
   chsh $SUDO_USER -s $(command -v zsh)
fi

#### TMUX ####
if [ ! $(command -v tmux) ]; then
   apt install tmux -y
fi

### Alacritty ###
if [ ! $(command -v alacritty) ]; then
   apt install alacritty -y
   update-alternatives --set x-terminal-emulator $(command -v alacritty)
fi

### Git ###
if [ $(apt list search --installed vim-gtk 2> /dev/null | wc -l) -lt 2 ]; then
   apt install vim-gtk -y
fi
# TODO retrieve SSH keys in someway

### Fonts ###
   git clone https://github.com/powerline/fonts.git
   cd fonts && ./install.sh
   cd ..

### Vim + Plug ###


if [ ! $(command -v git) ]; then
   apt install -y git
fi

# install plug
curl -fLo /home/$SUDO_USER/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
chown -R $SUDO_USER:$SUDO_USER /home/$SUDO_USER/.vim

vim +PlugInstall +qall

### Powerline Fonts ###
apt install -y fonts-powerline

